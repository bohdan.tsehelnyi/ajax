// ===================THEORY========================
/*
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX є технологією взаємодії інформацією між користувачем та сервером яку можна отримувати без перезагрузки сторінки. Це є дуже важливо, сокільки це вппливає в першу чергу на взаємодію з бекендом, а також безумовне покращення роботи нашого сайту. AJAX вступає в дію коли ми відправляємо дані на сервер, отримуємо їх з сервера, а також додаємо зміни або взагалі видаляємо частину інформації.
*/

// ==================PRACTICE=======================


const container = document.querySelector('.container');


fetch("https://ajax.test-danit.com/api/swapi/films")
.then(response => response.json())
.then(films => {
    films.forEach(film => {
        const box = document.createElement('div');
        box.classList = "box";
        const name = document.createElement('h2');
        name.style.textAlign = "center"
        name.innerText = film.name;
        const episode = document.createElement('h3');
        episode.style.cssText = "text-align: center; font-size: 30px;"
        episode.innerText = film.episodeId; 
        const crawl = document.createElement('p');
        crawl.innerText = film.openingCrawl;
        box.append(name, episode, crawl);
        container.append(box);
        const loader = document.createElement('div');
        loader.classList.add('loader');

        const charactersLoaded = [];

        film.characters.forEach(characterUrl => {
            const char = document.createElement('span');
            char.classList.add("character");
            box.append(char);
            fetch(characterUrl)
            .then(response => response.json())
            .then(character => {
                console.log(character);
                char.innerHTML = `${character.name},`;
                charactersLoaded.push(character.name);
                if (charactersLoaded.length === film.characters.length){
                    loader.style.display = "none"
                }
            });
        });
        box.append(loader);
    });
});
